package com.qa.testcases;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageactions.HomePageAction;
import com.qa.pageactions.LoginPageAction;
import com.qa.pageactions.OpenItemsPageAction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class NonComboTest extends TestBase{
	public NonComboTest() {
		super();
	}
	LoginPageAction loginPageAction;
	HomePageAction homePageAction;
	OpenItemsPageAction openItemsPageAction;
	
	@DataProvider
	public Object[][] getCOTestData(){
		Object data[][] = TestUtil.getTestData("NonCombo");
		return data;
	}
	
	@Test(dataProvider="getCOTestData")
	public void nonComboAWD_test(String scenarioName, String indExecute, String count,
			String businessArea, String workType, String status, String indPolicyNumber, String policyNumber, String insuredLastName, 
			String insuredSSN, String agentNumber, String agentLastName, String agentFirstName, String isAUWTkt,
			String isJointSSN, String jointSSN,
			String businessArea2, String workType2, String status2, String indPolicyNumber2, String policyNumber2, String insuredLastName2, 
			String insuredSSN2, String agentNumber2, String AgentLastName2, String agentFirstName2, String isAUWTkt2,
			String isJointSSN2, String jointSSN2,
			String businessArea3, String workType3, String status3, String indPolicyNumber3, String policyNumber3, String insuredLastName3, 
			String insuredSSN3, String agentNumber3, String AgentLastName3, String agentFirstName3, String isAUWTkt3,
			String isJointSSN3, String jointSSN3) throws IOException, InterruptedException, AWTException {
		
		
		extentTest = extent.startTest("Non Combo AWD " + scenarioName);
		if (indExecute.equalsIgnoreCase("No")) {
			extentTest.log(LogStatus.INFO, "Execute column is No in the data sheet");
	        throw new SkipException(scenarioName + " is Skipped");
	    }
		Thread.sleep(3000);
		initialization();
		extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
		Thread.sleep(3000);
		loginPageAction = new LoginPageAction();
		homePageAction = loginPageAction.login(prop.getProperty("loginID"), prop.getProperty("password"));
		
//		homePageAction.createNonComboWorkItem(businessArea, workType, status, indPolicyNumber, policyNumber,
//				insuredLastName, insuredSSN, agentNumber, agentLastName, agentFirstName,
//				isAUWTkt, isJointSSN, jointSSN);
		
		homePageAction.createWorkItem(count, businessArea, workType, status, indPolicyNumber, policyNumber, insuredLastName,
				insuredSSN, agentNumber, agentLastName, agentFirstName, isAUWTkt, isJointSSN, jointSSN,
				businessArea2, workType2, status2, indPolicyNumber2, policyNumber2, insuredLastName2, insuredSSN2,
				agentNumber2, AgentLastName2, agentFirstName2, isAUWTkt2, isJointSSN2, jointSSN2, 
				businessArea3, workType3, status3, indPolicyNumber3, policyNumber3, insuredLastName3, insuredSSN3, 
				agentNumber3, AgentLastName3, agentFirstName3, isAUWTkt3, isJointSSN3, jointSSN3);
		
//		String xpathName_Work1 = businessArea +" - "+ workType + " - "+status + " - ";
//		String xpathName_Work2 = businessArea2 +" - "+ workType2 + " - "+status2 + " - ";
//		String xpathName_Work3 = businessArea3 +" - "+ workType3 + " - "+status3 + " - ";
//		String xpathName_folder = businessAreaFolder +" - "+ folderType;
		Thread.sleep(5000);
		homePageAction.releaseNonCombo(count);
		homePageAction.signOff();
		extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(TestUtil.passedScreenshotForReport()));
	}
		
}
