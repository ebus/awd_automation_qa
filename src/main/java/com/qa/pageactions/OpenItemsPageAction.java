package com.qa.pageactions;

import com.qa.page.OpenItemsPage;

public class OpenItemsPageAction extends OpenItemsPage {
	public OpenItemsPageAction() {
		super();
	}
	
	//Actions:
	public void enterDataFields(String indPolicyNumber, String policyNumber, String insuredLastName, String insuredSSN, String agentNumber, String AgentLastName,
			String agentFirstName, String isAUWTkt,String isJointSSN, String jointSSN) {
		if(SelectionIndAsYes(indPolicyNumber, "Policy Number")) {
			EnterText(txtPolicyNumber, policyNumber, "PolicyNumber");
		}
		EnterText(txtInsuredLastName, insuredLastName, "txtInsuredLastName");
		EnterText(txtInsuredSSN, insuredSSN, "InsuredSSN");
		EnterText(txtAgentNumber, agentNumber, "AgentNumber");
		EnterText(txtAgentLastName, AgentLastName, "AgentLastName");
		EnterText(txtAgentFirstName, agentFirstName, "AgentFirstName");
		if(SelectionIndAsYes(isAUWTkt, "AUW Ticket")){
			ClickJSElement(cbAUW_Ticket, "AUW_Ticket");
		}
		
		if(SelectionIndAsYes(isJointSSN, "JointSSN")){
			EnterText(txtJointSSN, jointSSN, "JointSSN");
		}
//		ClickElement(element, strButtonName);
		ClickJSElement(btnUpdate, "Update");
		takeScreenshot("Items Details");
		
	}
}
