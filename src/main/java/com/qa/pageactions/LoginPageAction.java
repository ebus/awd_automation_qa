package com.qa.pageactions;

import com.qa.page.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageAction extends LoginPage{
	public LoginPageAction() {
		super();
	}
	
	//Actions:
	public HomePageAction login(String userID, String password) throws InterruptedException {
		extentTest.log(LogStatus.INFO, "-----Login Page-----");
		EnterText(txtUserID, userID, "UserID");
		EnterText(txtPassword, password, "Password");	
		ClickJSElement(btnSignOn, "SignOn");
		Thread.sleep(5000);
		return new HomePageAction();
	}
}
