package com.qa.pageactions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.qa.page.HomePage;
import com.relevantcodes.extentreports.LogStatus;

public class HomePageAction extends HomePage{
	public HomePageAction() {
		super();
	}
	static WebElement btnUnderFolder_Work_1;
	static WebElement btnUnderFolder_Work_2;
	static WebElement btnUnderFolder_Work_3;
	static WebElement btnFolder;
	//Actions:
	
	public void createWorkItem(String count, String businessArea, String workType, String status, String indPolicyNumber, 
			String policyNumber, String insuredLastName, String insuredSSN, String agentNumber, String AgentLastName,
			String agentFirstName, String isAUWTkt,String isJointSSN, String jointSSN,
			String businessArea2, String workType2, String status2, String indPolicyNumber2, String policyNumber2, String insuredLastName2, 
			String insuredSSN2, String agentNumber2, String AgentLastName2, String agentFirstName2, String isAUWTkt2,
			String isJointSSN2, String jointSSN2,
			String businessArea3, String workType3, String status3, String indPolicyNumber3, String policyNumber3, String insuredLastName3, 
			String insuredSSN3, String agentNumber3, String AgentLastName3, String agentFirstName3, String isAUWTkt3,
			String isJointSSN3, String jointSSN3) throws InterruptedException {
		switch (count) {
			case "1":
			{
				clickCreateWork();
				selectCreateWorkOptions(businessArea, workType, status);
				selectCreateResults(btnCreateResults_Work_1);
				enterDataFields(indPolicyNumber, policyNumber, insuredLastName, insuredSSN, agentNumber, AgentLastName, agentFirstName, 
						isAUWTkt, isJointSSN, jointSSN);
				break;
			}
			case "2":
			{
				workItem2(businessArea, workType, status, indPolicyNumber, policyNumber, insuredLastName, insuredSSN,
						agentNumber, AgentLastName, agentFirstName, isAUWTkt, isJointSSN, jointSSN, businessArea2,
						workType2, status2, indPolicyNumber2, policyNumber2, insuredLastName2, insuredSSN2,
						agentNumber2, AgentLastName2, agentFirstName2, isAUWTkt2, isJointSSN2, jointSSN2);
				
				break;
			}
			case "3":
			{
				workItem2(businessArea, workType, status, indPolicyNumber, policyNumber, insuredLastName, insuredSSN,
						agentNumber, AgentLastName, agentFirstName, isAUWTkt, isJointSSN, jointSSN, businessArea2,
						workType2, status2, indPolicyNumber2, policyNumber2, insuredLastName2, insuredSSN2,
						agentNumber2, AgentLastName2, agentFirstName2, isAUWTkt2, isJointSSN2, jointSSN2);
				
				clickCreateWork();
				selectCreateWorkOptions(businessArea3, workType3, status3);
				selectCreateResults(btnCreateResults_Work_3);
				enterDataFields(indPolicyNumber3, policyNumber3, insuredLastName3, insuredSSN3, agentNumber3, AgentLastName3, agentFirstName3, 
						isAUWTkt3, isJointSSN3, jointSSN3);
				break;
			}
		}
	}

	private void workItem2(String businessArea, String workType, String status, String indPolicyNumber,
			String policyNumber, String insuredLastName, String insuredSSN, String agentNumber, String AgentLastName,
			String agentFirstName, String isAUWTkt, String isJointSSN, String jointSSN, String businessArea2,
			String workType2, String status2, String indPolicyNumber2, String policyNumber2, String insuredLastName2,
			String insuredSSN2, String agentNumber2, String AgentLastName2, String agentFirstName2, String isAUWTkt2,
			String isJointSSN2, String jointSSN2) throws InterruptedException {
		clickCreateWork();
		selectCreateWorkOptions(businessArea, workType, status);
		selectCreateResults(btnCreateResults_Work_1);
		enterDataFields(indPolicyNumber, policyNumber, insuredLastName, insuredSSN, agentNumber, AgentLastName, agentFirstName, 
				isAUWTkt, isJointSSN, jointSSN);
		
		clickCreateWork();
		selectCreateWorkOptions(businessArea2, workType2, status2);
		selectCreateResults(btnCreateResults_Work_2);
		enterDataFields(indPolicyNumber2, policyNumber2, insuredLastName2, insuredSSN2, agentNumber2, AgentLastName2, agentFirstName2, 
				isAUWTkt2, isJointSSN2, jointSSN2);
	}
	public void clickCreateWork() throws InterruptedException {
//		extentTest.log(LogStatus.INFO, "-----Home Page-----");
		ClickJSElement(optionCreate, "Create Button");
		ClickJSElement(btnCreateWork, "CreateWork Button");
		Thread.sleep(3000);
	}
	
	public void clickCreateFolder() throws InterruptedException {
//		extentTest.log(LogStatus.INFO, "-----Home Page-----");
		ClickJSElement(optionCreate, "Create Folder");
		ClickJSElement(btnCreateFolder, "CreateWork Folder");
		Thread.sleep(3000);
	}
	
	public void selectCreateFolderOptions(String businessArea_Folder, String folderType) throws InterruptedException {
		ComboSelectValue(dropdownBusinessArea_Folder, businessArea_Folder, "Business Area Folder");
		ComboSelectValue(dropdownFolderType, folderType, "FolderType");
		ClickJSElement(btnCreate_Folder, "Create Button FloderType");
		Thread.sleep(5000);
	}
	
	public void selectCreateWorkOptions(String businessArea, String workType, String status) throws InterruptedException {
		ComboSelectValue(dropdownBusinessArea, businessArea, "Business Area");
		ComboSelectValue(dropdownWorkType, workType, "WorkType");
		ComboSelectValue(dropdownStatus, status, "Status");
		ClickJSElement(btnCreate, "Create Button");
		Thread.sleep(3000);
	}
	
	public OpenItemsPageAction selectCreateResults(WebElement btnCreateResults) throws InterruptedException {
		ClickJSElement(btnCreateResults, "CreateResults");
		Thread.sleep(2000);
		ClickJSElement(linkOpen, "Open");
		Thread.sleep(5000);
		return new OpenItemsPageAction();
	}
	
	public void copyWorkItemsToFolder(String count, String xpathName_Work1, String xpathName_Work2, String xpathName_Work3,
			String xpathName_folder) throws InterruptedException {
		String xpathPart1 = "//a[contains(text(), '";
		String xpathPart3 = "')]/../../div[@class='awdObjectAction ']";
		String finalXpath_Work1 = xpathPart1 + xpathName_Work1 + xpathPart3;
		String finalXpath_Work2 = xpathPart1 + xpathName_Work2 + xpathPart3;
		String finalXpath_Work3 = xpathPart1 + xpathName_Work3 + xpathPart3;
		String finalXpath_folder = xpathPart1 + xpathName_folder + xpathPart3;
		
		try {
			btnUnderFolder_Work_1 = driver.findElement(By.xpath(finalXpath_Work1));
			btnUnderFolder_Work_2 = driver.findElement(By.xpath(finalXpath_Work2));
			btnUnderFolder_Work_3 = driver.findElement(By.xpath(finalXpath_Work3));
			
		}catch(Exception e) {
			
		}	
		switch(count) {
			case "1":
			{
				copyWorkItem(btnUnderFolder_Work_1);
				pasteToFolder(finalXpath_folder);
				break;
			}
			case "2":
			{	
				copyWorkItem(btnUnderFolder_Work_1);
				pasteToFolder(finalXpath_folder);
				copyWorkItem(btnUnderFolder_Work_2);
				pasteToFolder(finalXpath_folder);
				break;
			}
			case "3":
			{
				copyWorkItem(btnUnderFolder_Work_1);
				pasteToFolder(finalXpath_folder);
				copyWorkItem(btnUnderFolder_Work_2);
				pasteToFolder(finalXpath_folder);
				copyWorkItem(btnUnderFolder_Work_3);
				pasteToFolder(finalXpath_folder);
				break;
			}
		}
		
	}

	
	public void copyWorkItem(WebElement btnCreateResults_Copy) throws InterruptedException {
		ClickJSElement(btnCreateResults_Copy, "CreateResults");
		Thread.sleep(2000);
		ClickJSElement(linkCopy, "Copy");
		Thread.sleep(3000);
	}
	
	public void pasteToFolder(String finalXpath_folder) throws InterruptedException {
		try {
			btnFolder = driver.findElement(By.xpath(finalXpath_folder));
		}catch (Exception e) {
			
		}
		
		ClickJSElement(btnFolder, "CreateResults Foldertype");
		Thread.sleep(2000);
		ClickJSElement(linkPaste, "Paste");
		Thread.sleep(3000);
	}
	
	public void releaseWorkItemUnderFolder(String count, String xpathName_Work1, String xpathName_Work2, String xpathName_Work3) throws InterruptedException {
		String xpathPart1 = "(//a[contains(text(), '";
		String xpathPart3 = "')]/../../div[@class='awdObjectAction '])[2]";
		String finalXpath_Work1 = xpathPart1 + xpathName_Work1 + xpathPart3;
		String finalXpath_Work2 = xpathPart1 + xpathName_Work2 + xpathPart3;
		String finalXpath_Work3 = xpathPart1 + xpathName_Work3 + xpathPart3;
		try {
			btnUnderFolder_Work_1 = driver.findElement(By.xpath(finalXpath_Work1));
			btnUnderFolder_Work_2 = driver.findElement(By.xpath(finalXpath_Work2));
			btnUnderFolder_Work_3 = driver.findElement(By.xpath(finalXpath_Work3));
		}catch(Exception e) {
			
		}
		
		scrollIntoView(btnUnderFolder_Work_1, driver);
		switch (count) {
			case "1":
			{
				ClickJSElement(btnUnderFolder_Work_1, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
			case "2":
			{
				ClickJSElement(btnUnderFolder_Work_1, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				ClickJSElement(btnUnderFolder_Work_2, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
			case "3":
			{
				ClickJSElement(btnUnderFolder_Work_1, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				ClickJSElement(btnUnderFolder_Work_2, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				ClickJSElement(btnUnderFolder_Work_3, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
		}
		takeScreenshot("Final screenshot");
		Thread.sleep(2000);
	}
	
	public void enterDataFields(String indPolicyNumber, String policyNumber, String insuredLastName, String insuredSSN, String agentNumber, String AgentLastName,
			String agentFirstName, String isAUWTkt,String isJointSSN, String jointSSN) {
		if(SelectionIndAsYes(indPolicyNumber, "Policy Number")) {
			EnterText(txtPolicyNumber, policyNumber, "PolicyNumber");
		}
		EnterText(txtInsuredLastName, insuredLastName, "txtInsuredLastName");
		EnterText(txtInsuredSSN, insuredSSN, "InsuredSSN");
		EnterText(txtAgentNumber, agentNumber, "AgentNumber");
		EnterText(txtAgentLastName, AgentLastName, "AgentLastName");
		EnterText(txtAgentFirstName, agentFirstName, "AgentFirstName");
		if(SelectionIndAsYes(isAUWTkt, "AUW Ticket")){
			ClickJSElement(cbAUW_Ticket, "AUW_Ticket");
		}
		if(SelectionIndAsYes(isJointSSN, "JointSSN")){
			EnterText(txtJointSSN, jointSSN, "JointSSN");
		}
		ClickJSElement(btnUpdate, "Update");
		takeScreenshot("Items Details");		
	}
	
	public void createNonComboWorkItem(String businessArea, String workType, String status,
			String indPolicyNumber, String policyNumber, String insuredLastName, String insuredSSN, String agentNumber, 
			String agentLastName, String agentFirstName, 
			String isAUWTkt, String isJointSSN, String jointSSN) throws InterruptedException {
		clickCreateWork();
		selectCreateWorkOptions(businessArea, workType, status);
		selectCreateResults(btnCreateResults_Work_1);
		enterDataFields(indPolicyNumber, policyNumber, insuredLastName, insuredSSN, agentNumber, agentLastName, agentFirstName, 
				isAUWTkt, isJointSSN, jointSSN);
	}
	
	public void releaseNonCombo(String count) throws InterruptedException {
		
		
		switch (count) {
			case "1":
			{
				ClickJSElement(btnCreateResults_Work_1_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
			case "2":
			{
				ClickJSElement(btnCreateResults_Work_1_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				ClickJSElement(btnCreateResults_Work_2_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
			case "3":
			{
				ClickJSElement(btnCreateResults_Work_1_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(5000);
				ClickJSElement(btnCreateResults_Work_2_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(5000);
				ClickJSElement(btnCreateResults_Work_3_NonCombo, "CreateResults WorkItem");
				Thread.sleep(2000);
				ClickJSElement(linkRelease, "Release");
				Thread.sleep(3000);
				break;
			}
		}
		takeScreenshot("Final screenshot");
		Thread.sleep(2000);
	}
	public void signOff() throws InterruptedException, AWTException {
		Thread.sleep(2000);
		ClickJSElement(btnSignOff, "Sign Off");
		Thread.sleep(3000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
	}
	
}
