package com.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HomePage extends GenericFunction{
	//Initializing the Page Objects:
	public HomePage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(xpath = "//img[@id='undefined_img' and @alt='Add Create']") 
	public WebElement optionCreate;

	@FindBy(xpath = "//a[contains(text(), 'Create Work')]") 
	public WebElement btnCreateWork;
	
	@FindBy(xpath = "//a[contains(text(), 'Create Folder')]") 
	public WebElement btnCreateFolder;

	@FindBy(xpath = "//select[@id='awd_cw_ba']")  //select[@id='awd_cw_ba'] select#awd_cw_ba awd_cf_ba
	public WebElement dropdownBusinessArea;
	
	@FindBy(css = "#awd_cw_wrkt") 				//select[@id='awd_cw_wrkt']
	public WebElement dropdownWorkType;
	
	@FindBy(css = "#awd_cw_stat") 				//select[@id='awd_cw_stat']
	public WebElement dropdownStatus;
	
	@FindBy(xpath = "//button[@id='awd_cw_create_btn']") 
	public WebElement btnCreate;
	
	@FindBy(xpath = "//select[@id='awd_cf_ba']")  
	public WebElement dropdownBusinessArea_Folder;
	
	@FindBy(css = "#awd_cf_fold") 				
	public WebElement dropdownFolderType;
	
	@FindBy(xpath = "//button[@id='awd_cf_create_btn']") 
	public WebElement btnCreate_Folder;
	
	@FindBy(xpath = "(//a[@title='case link']/../../div/img[@class='dstActionButtonImage'])") 
	public WebElement btnCreateResults_Work_1;
	
	@FindBy(xpath = "(//a[@title='case link']/../../div/img[@class='dstActionButtonImage'])[2]") 
	public WebElement btnCreateResults_Work_2;
	
	@FindBy(xpath = "(//a[@title='case link']/../../div/img[@class='dstActionButtonImage'])[3]") 
	public WebElement btnCreateResults_Work_3;
	
	@FindBy(xpath = "(//div/img[@class='dstActionButtonImage'])[51]") 
	public WebElement btnCreateResults_Work_1_NonCombo;
	
	@FindBy(xpath = "(//div/img[@class='dstActionButtonImage'])[52]") 
	public WebElement btnCreateResults_Work_2_NonCombo;
	
	@FindBy(xpath = "(//div/img[@class='dstActionButtonImage'])[53]") 
	public WebElement btnCreateResults_Work_3_NonCombo;
	
	@FindBy(xpath = "//a[@title='caselink']/../../div/img[@class='dstActionButtonImage']") 
	public WebElement btnCreateResults_Copy_1;
	
	@FindBy(xpath = "(//a[@title='case link']/../../div/img[@class='dstActionButtonImage'])[2]") 
	public WebElement btnCreateResults_Copy_2;
	
	@FindBy(xpath = "//a[contains(text(),'Open')]") 
	public WebElement linkOpen;
	
	@FindBy(xpath = "//a[contains(text(),'Copy')]") 
	public WebElement linkCopy;
	
	@FindBy(xpath = "//a[@title='folder link']/../../div/img[@class='dstActionButtonImage']") 
	public WebElement btnCreateResults_Folder;
	
	@FindBy(xpath = "//a[@title='folderlink']/../../div/img[@class='dstActionButtonImage']") 
	public WebElement btnCreateResults_Folder_2;
	
	
	@FindBy(xpath = "(//a[@title='case link']/../../div/img[@class='dstActionButtonImage'])[2]") 
	public WebElement btnCreateResults_WorkItem;
	
	@FindBy(xpath = "//a[contains(text(),'Paste')]") 
	public WebElement linkPaste;
	
	@FindBy(xpath = "//a[contains(text(),'Release')]") 
	public WebElement linkRelease;
	
	@FindBy(xpath = "//input[@title='Policy Number']") 
	public WebElement txtPolicyNumber;
	
	@FindBy(xpath = "//input[@title='Insured Last Name']") 
	public WebElement txtInsuredLastName;
	
	@FindBy(xpath = "//input[@title='Insured SSN']") 
	public WebElement txtInsuredSSN;
	
	@FindBy(xpath = "//input[@title='DEFAULT HELP TEXT']") 
	public WebElement txtAgentNumber;
	
	@FindBy(xpath = "//input[@name='AGTL']") 
	public WebElement txtAgentLastName;
	
	@FindBy(xpath = "//input[@name='AGTF']") 
	public WebElement txtAgentFirstName;
	
	@FindBy(xpath = "//input[@name='AUWI']") 
	public WebElement cbAUW_Ticket;
	
	@FindBy(xpath = "//input[@name='CSSN']") 
	public WebElement txtJointSSN;
	
	@FindBy(xpath = "//button[contains(text(),'Update')]") 
	public WebElement btnUpdate;
	
	@FindBy(css = "#awd_logoff_link") 				
	public WebElement btnSignOff;
}
