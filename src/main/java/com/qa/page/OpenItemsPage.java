package com.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class OpenItemsPage extends GenericFunction{
	//Initializing the Page Objects:
	public OpenItemsPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR: 
	@FindBy(xpath = "//input[@title='Policy Number']") 
	public WebElement txtPolicyNumber;
	
	@FindBy(xpath = "//input[@title='Insured Last Name']") 
	public WebElement txtInsuredLastName;
	
	@FindBy(xpath = "//input[@title='Insured SSN']") 
	public WebElement txtInsuredSSN;
	
	@FindBy(xpath = "//input[@title='DEFAULT HELP TEXT']") 
	public WebElement txtAgentNumber;
	
	@FindBy(xpath = "//input[@name='AGTL']") 
	public WebElement txtAgentLastName;
	
	@FindBy(xpath = "//input[@name='AGTF']") 
	public WebElement txtAgentFirstName;
	
	@FindBy(xpath = "//input[@name='AUWI']") 
	public WebElement cbAUW_Ticket;
	
	@FindBy(xpath = "//input[@name='CSSN']") 
	public WebElement txtJointSSN;
	
	@FindBy(xpath = "//button[contains(text(),'Update')]") 
	public WebElement btnUpdate;
}
